#include "DialogueActions.h"
#include <string>
#include "DialogueMgr.h"


bool NewStatementAction::Execute(const float ticks){
	Statement** statement = DialogueMgr::getInstance().getTempStatement();
	*statement = new Statement(statementText, statementID);

	return 0;
}

bool EndStatementAction::Execute(const float ticks) {
	Dialogue** dialogue = DialogueMgr::getInstance().getDialogue();
	Statement** statement = DialogueMgr::getInstance().getTempStatement();

	(*dialogue)->addStatement(*statement);
	*statement = NULL;

	return 0;
}

bool ReadNextLineAction::Execute(const float ticks) {
	
	DialogueMgr::getInstance().ReadFileLine();

	return 0;
}

bool EndReadingDialogueAction::Execute(const float ticks) {
	
	DialogueMgr::getInstance().EndReadingDialogue();

	return 0;
}
