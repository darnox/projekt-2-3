#pragma once
#include <vector>
#include "DialogueActions.h"
#include "DCAction.h"
#include "Observer.h"
#include <string>

//class DCAction;

class Statement: public Observer{
public:
	Statement() {};
	Statement(std::string statementText, int statementID) : statementText(statementText), statementID(statementID), isAvailable(1) {};
	~Statement();
	void OnNotify(DCAction * subject);
	void OnNotify(Subject* subject) {};
	void CheckConditions() {

	}

	int getID() {
		return statementID;
	}

	std::vector<int>* getTmpNext() {
		return &tmpNextStatements;
	}

	void AddCondition(DCAction* condition) { // TODO: move to cpp
		condition->AddObserver(this);
		conditions.push_back(condition);
	}
	void AddAction(DCAction* action) {
		conditions.push_back(action);
	}
	void AddTmpNext(int next) {
		tmpNextStatements.push_back(next);
	}

	void AddNext(Statement* statement) {
		nextStatements.push_back(statement);
	}

	std::string getStatementText() {
		return statementText;
	}
private:
	bool isAvailable;
	int statementID;
	std::string statementText;
	std::vector<Statement*> nextStatements;
	std::vector<int> tmpNextStatements;
	std::vector<DCAction*> conditions; 
	std::vector<DialogueAction*> actions;
};

