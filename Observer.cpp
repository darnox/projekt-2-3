#include "Observer.h"

void Subject::Notify() {
	for (auto observer: observers)
		observer->OnNotify(this);
}

void Subject::AddObserver(Observer* observer) {
	observers.push_back(observer);
}
