#pragma once
enum class DialogueActions { NewStatement, EndStatement, ReadNextLine, EndReadingDialogue};

class DialogueAction{
public:
	DialogueAction() {}
	virtual ~DialogueAction() {};
	static DialogueAction* CreateObject();
	virtual bool Execute(const float ticks = 0) = 0;
private:
	int currentState;
	float time;
};


