#pragma once
#include "Statement.h"
#include <vector>


class Statement;

class Dialogue{
public:
	Dialogue();
	~Dialogue();

	void addStatement(Statement* statement) {
		statements.push_back(statement);
		if (currentStatement == NULL) currentStatement = statements[0];
	}

	void getNextStatement();
	Statement* getCurrentStatement() { return currentStatement; }
	void MakeConnections();

private:
	Statement* Dialogue::FindStatementWithID(int id);

	std::vector<Statement*> statements;
	Statement* currentStatement = NULL;
};

