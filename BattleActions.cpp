#include "BattleActions.h"
#include "BattleMgr.h"
#include "src\windowmgr.h"
#include <fstream>
#include "src\actor.h"
#include "src\statblock.h"


#define BMI BattleMgr::getInstance()
#define CreateAction(x) BMI.AddAction(ActionFactory::getInstance().CreateObj(x))
#define TextDelay 1000
typedef Factory<BattleActions, BattleAction> ActionFactory;

bool StartBattleAction::Execute(const float ticks) {
	CreateAction(BattleActions::Attack);
	return 0;
}
bool EndBattleAction::Execute(const float ticks) {
	BMI.EndBattle();
	return 0;
}

bool DelayAction::Execute(const float ticks) {
	time -= ticks;
	if (time < 0) return 0;
	else return 1;
}

bool AttackAction::Execute(const float ticks){
	std::string name = BMI.getEnemy()->GetStatBlock().GetName();
	if (currentState == 0) { // Show text
		std::string info;
		if (BMI.getPlayer() == BMI.getActivePlayer()) {
			info = "You are attacking " + name;
		}
		else {
			info = name + " is attacking you.";
		}
		BMI.ShowCombatText(info);
		currentState++;
	}	
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Attack position
		BMI.SetActorPose(BMI.getActivePlayer(), BattleAniFrames::Attack);
		currentState++;
	}
	else if (currentState == 2) { // Wait
		time += ticks;
		if (time > TextDelay) {

			currentState++;
			time = 0;
		}
	}
	else if (currentState == 3) { // Hide text
		BMI.HideCombatText();
		currentState++;
	}
	else if (currentState == 4) { // Calculate hit type
		int randomNumber = rand() % 100;

		if (randomNumber < 10) CreateAction(BattleActions::MissedAttack);
		else if (randomNumber <= 90) CreateAction(BattleActions::StandardAttack);
		else CreateAction(BattleActions::CriticalAttack);

		return 0;
	}
	return 1;
}

bool StandardAttackAction::Execute(const float ticks) {
	std::string name = BMI.getEnemy()->GetStatBlock().GetName();
	if (currentState == 0) { // Show text
		BMI.SetActorPose(BMI.getActivePlayer(), BattleAniFrames::Idle);
		std::string info;
		if (BMI.getPlayer() == BMI.getActivePlayer()) info = "You";
		else info = name;	
		info += " made a Standard Attack";
		BMI.ShowCombatText(info);
		currentState++;
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;

		// Calculate damage
		int damage = BMI.getActivePlayer()->GetStatBlock().GetAttack();

		BMI.AddAction(ActionFactory::getInstance().CreateObj<int, std::string>(BattleActions::AddExp, 1, "standard hit"));
		BMI.AddAction(ActionFactory::getInstance().CreateObj<int>(BattleActions::TakeDamage, damage));

		return 0;
	}
	return 1;
}

bool CriticalAttackAction::Execute(const float ticks) {
	std::string name = BMI.getEnemy()->GetStatBlock().GetName();
	if (currentState == 0) { // Show text
		BMI.SetActorPose(BMI.getActivePlayer(), BattleAniFrames::Idle);
		std::string info;
		if (BMI.getPlayer() == BMI.getActivePlayer()) info = "You";
		else info = name;
		info += " made a Critical Attack";
		BMI.ShowCombatText(info);
		currentState++;
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;

		// Calculate damage
		int damage = (int)ceil(BMI.getActivePlayer()->GetStatBlock().GetAttack() * 1.25);

		BMI.AddAction(ActionFactory::getInstance().CreateObj<int, std::string>(BattleActions::AddExp, 2, "critical hit"));
		BMI.AddAction(ActionFactory::getInstance().CreateObj<int>(BattleActions::TakeDamage, damage));

		return 0;
	}
	return 1;
}

bool MissedAttackAction::Execute(const float ticks) {
	std::string name = BMI.getEnemy()->GetStatBlock().GetName();
	if (currentState == 0) { // Show text
		BMI.SetActorPose(BMI.getActivePlayer(), BattleAniFrames::Idle);
		std::string info;
		if (BMI.getPlayer() == BMI.getActivePlayer()) info = "You";
		else info = name;
		info += " missed an attack";
		BMI.ShowCombatText(info);
		currentState++;
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;
		CreateAction(BattleActions::CheckCounterAttack);
		return 0;
	}
	return 1;
}

bool TakeDamageAction::Execute(const float ticks) {
	std::string name = BMI.getEnemy()->GetStatBlock().GetName();

	int dmg = (int)ceil(damage - BMI.getOponentPlayer()->GetStatBlock().GetDefense());
	if (dmg < 0) dmg = 0;

	if (currentState == 0) { // Show text
		std::string info;
		if (BMI.getPlayer() != BMI.getActivePlayer()) {
			info = "You";
		}
		else info = name;
		info += " lost " + std::to_string(dmg) + " hp";

		BMI.getOponentPlayer()->GetStatBlock().TakeDamage(dmg);

		BMI.ShowCombatText(info);
		currentState++;
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;
		CreateAction(BattleActions::CheckWin);
		return 0;
	}
	return 1;
}

bool AddExpAction::Execute(const float ticks) {
	if (currentState == 0) { // Show text
		std::string info;

		if(BMI.getActivePlayer()->GetStatBlock().AddExp(exp)) CreateAction(BattleActions::LevelUp);

		if (BMI.getPlayer() == BMI.getActivePlayer()) {
			info = "You've recived " + std::to_string(exp) + " exp for "+name;
			BMI.ShowCombatText(info);
			currentState++;
		}
		else currentState = 3;	
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;
	}
	else if (currentState == 3) { // Go to another action
		currentState++;
		return 0;
	}
	return 1;
}

bool CheckWinAction::Execute(const float ticks) {
	if (currentState == 0) { // Show text
		std::string info;
		if (!BMI.getOponentPlayer()->GetStatBlock().IsAlive()) {
			if (BMI.getPlayer() == BMI.getActivePlayer()) {
				info = "You won the battle!";
			}
			else {
				info = "You lost the battle...";
			}

			BMI.getActivePlayer()->GetStatBlock().AddKill();

			BMI.ShowCombatText(info);
			currentState++;
		}
		else {
			currentState = 3;
		}
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;
	}
	else if (currentState == 3) { // Go to another action
		if (!BMI.getOponentPlayer()->GetStatBlock().IsAlive()) {
			if (BMI.getPlayer() == BMI.getActivePlayer()) {
				BMI.AddAction(ActionFactory::getInstance().CreateObj<int>(BattleActions::Delay, 2000));
				BMI.AddAction(ActionFactory::getInstance().CreateObj<int, std::string>(BattleActions::AddExp, 5, "winning the battle"));
				CreateAction(BattleActions::TakeItems);
				CreateAction(BattleActions::EndBattle);
			}
			else {
				CreateAction(BattleActions::EndBattle);
			}

		}
		else CreateAction(BattleActions::CheckCounterAttack);
		currentState++;
		

		return 0;
	}

	return 1;
}

bool CheckCounterAttackAction::Execute(const float ticks) {
	
	int random = rand() % 100;
	
	if (random <= 5) {
		CreateAction(BattleActions::ChangeActor);
		CreateAction(BattleActions::CounterAttack);
	}
	else {
		CreateAction(BattleActions::ChangeActor);
		CreateAction(BattleActions::NextRound);
	}

	return 0;
}

bool ChangeActorAction::Execute(const float ticks) {
	BMI.ChangeActive();
	return 0;
}

bool CounterAttackAction::Execute(const float ticks) {
	if (currentState == 0) { // Show text
		std::string info;
		if (BMI.getPlayer() == BMI.getActivePlayer()) {
			info = "You are making counterattack";
		}
		else {
			std::string name = BMI.getActivePlayer()->GetStatBlock().GetName();
			info = name + " is making counterattack";
		}

		BMI.ShowCombatText(info);
		currentState++;
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;

		CreateAction(BattleActions::Attack);
		return 0;
	}

	return 1;
}

bool NextRoundAction::Execute(const float ticks) {
	CreateAction(BattleActions::Attack);
	return 0;
}

bool TakeItemsAction::Execute(const float ticks) {
	if (currentState == 0) { // Show text
		std::string info;
		Item* itm = BMI.getOponentPlayer()->GetStatBlock().GetInventory()->GetEquippedWeapon();

		if (itm != NULL) {
			info = "You founded an item: "+itm->GetName();
			BMI.getActivePlayer()->GetStatBlock().AddItem(itm->GetName());

			BMI.ShowCombatText(info);
			currentState++;
		}
		else currentState = 3;	
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;
	}
	else if (currentState == 3) { // Go to another action
		return 0;
	}

	return 1;
}

bool LevelUpAction::Execute(const float ticks) {
	if (currentState == 0) { // Show text
		std::string info;
		if (BMI.getPlayer() == BMI.getActivePlayer()) {
			info = "You leveled up!";
			BMI.ShowCombatText(info);
			currentState++;
		}
		else currentState = 3;
	}
	else if (currentState == 1 && BMI.GetCombatText()->AtEndOfString()) { // Wait
		time += ticks;
		if (time > TextDelay) {
			currentState++;
			time = 0;
		}
	}
	else if (currentState == 2) { // Hide text
		BMI.HideCombatText();
		currentState++;
	}
	else if (currentState == 3) { // Go to another action
		currentState++;
		return 0;
	}
	return 1;
}

bool ResetStatsAction::Execute(const float ticks) {
	

	return 0;
}