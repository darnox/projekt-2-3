#pragma once
#include <vector>

class Subject;

class Observer{
public:
	Observer() {};
	~Observer() {};
	virtual void OnNotify(Subject* subject) = 0;
};

class Subject {
public:
	Subject() {};
	virtual ~Subject() {};
	virtual void AddObserver(Observer*);
	virtual void Notify();
private:
	std::vector<Observer*> observers;
};

