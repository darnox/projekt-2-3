#pragma once
#include "BattleAction.h"
#include <vector>

// Different Actions

class StartBattleAction : public BattleAction {
public:
	StartBattleAction(float time) : time(time) {};
	virtual ~StartBattleAction() {};
	static BattleAction* CreateObject(float time = 0) { return new StartBattleAction(time); }
	bool Execute(const float ticks = 0);
private:
	float time;
};

class EndBattleAction : public BattleAction {
public:
	EndBattleAction() {};
	virtual ~EndBattleAction() {};
	static BattleAction* CreateObject() { return new EndBattleAction; }
	bool Execute(const float ticks = 0);
};

class AttackAction : public BattleAction {
public:
	AttackAction(float ticks = 0): currentState(0), time(ticks) {};
	virtual ~AttackAction() {};
	static BattleAction* CreateObject() { return new AttackAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState;
	float time;
};

class StandardAttackAction : public BattleAction {
public:
	StandardAttackAction(float ticks = 0) : currentState(0), time(ticks) {};
	virtual ~StandardAttackAction() {};
	static BattleAction* CreateObject() { return new StandardAttackAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time;
};

class CriticalAttackAction : public BattleAction {
public:
	CriticalAttackAction(float ticks = 0) : currentState(0), time(ticks) {};
	virtual ~CriticalAttackAction() {};
	static BattleAction* CreateObject() { return new CriticalAttackAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time;
};

class MissedAttackAction : public BattleAction {
public:
	MissedAttackAction(float ticks = 0) : currentState(0), time(ticks) {};
	virtual ~MissedAttackAction() {};
	static BattleAction* CreateObject() { return new MissedAttackAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class TakeDamageAction : public BattleAction {
public:
	TakeDamageAction(int damage): damage(damage) {};
	virtual ~TakeDamageAction() {};
	static BattleAction* CreateObject(int damage) { return new TakeDamageAction(damage); }
	bool Execute(const float ticks = 0);
private:
	float damage;
	int currentState = 0;
	float time = 0;
};

class CheckWinAction : public BattleAction {
public:
	CheckWinAction() {};
	virtual ~CheckWinAction() {};
	static BattleAction* CreateObject() { return new CheckWinAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class AddExpAction : public BattleAction {
public:
	AddExpAction(int exp, std::string name): exp(exp), name(name) {};
	virtual ~AddExpAction() {};
	static BattleAction* CreateObject(int exp, std::string name) { return new AddExpAction(exp, name); }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
	int exp;
	std::string name;
};

class CounterAttackAction : public BattleAction {
public:
	CounterAttackAction() {};
	virtual ~CounterAttackAction() {};
	static BattleAction* CreateObject() { return new CounterAttackAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class ChangeActorAction : public BattleAction {
public:
	ChangeActorAction() {};
	virtual ~ChangeActorAction() {};
	static BattleAction* CreateObject() { return new ChangeActorAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class TakeGoldAction : public BattleAction {
public:
	TakeGoldAction() {};
	virtual ~TakeGoldAction() {};
	static BattleAction* CreateObject() { return new TakeGoldAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class TakeItemsAction : public BattleAction {
public:
	TakeItemsAction() {};
	virtual ~TakeItemsAction() {};
	static BattleAction* CreateObject() { return new TakeItemsAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class DelayAction : public BattleAction {
public:
	DelayAction(float time) : time(time) {};
	virtual ~DelayAction() {};
	static BattleAction* CreateObject(float time = 0) {
		return new DelayAction(time); 
	}
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class CheckCounterAttackAction : public BattleAction {
public:
	CheckCounterAttackAction() {};
	virtual ~CheckCounterAttackAction() {};
	static BattleAction* CreateObject() { return new CheckCounterAttackAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class NextRoundAction : public BattleAction {
public:
	NextRoundAction() {};
	virtual ~NextRoundAction() {};
	static BattleAction* CreateObject() { return new NextRoundAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class LevelUpAction : public BattleAction {
public:
	LevelUpAction() {};
	virtual ~LevelUpAction() {};
	static BattleAction* CreateObject() { return new LevelUpAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};

class ResetStatsAction : public BattleAction {
public:
	ResetStatsAction() {};
	virtual ~ResetStatsAction() {};
	static BattleAction* CreateObject() { return new ResetStatsAction; }
	bool Execute(const float ticks = 0);
private:
	int currentState = 0;
	float time = 0;
};