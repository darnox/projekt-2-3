#include "outro.h"
#include "inputmgr.h"
#include "gameobjectmgr.h"

GameOutro::GameOutro()
{
	m_logo = m_black;
	m_fadePct = 0.0f;
	m_fadeTracker.SetRate(50);

	m_openingText.SetOffset(120, 120);
	m_openingText.SetString("GAME OVER");
}

GameOutro::~GameOutro()
{

}

void GameOutro::ProcessInput()
{
	if (InputMgr::Instance().WasPressed(GEN_A) || 
		InputMgr::Instance().WasPressed(GEN_B) || 
		InputMgr::Instance().WasPressed(GEN_C) || 
		InputMgr::Instance().WasPressed(VK_ESCAPE)) 
	{
		GameObjectMgr::Instance().QuitGame();
	}
}