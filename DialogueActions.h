#pragma once
#include "DialogueAction.h"
#include <string>


class NewStatementAction : public DialogueAction {
public:
	NewStatementAction(std::string statementText, int statementID) : statementText(statementText), statementID(statementID) {};
	virtual ~NewStatementAction() {};
	static NewStatementAction* CreateObject(std::string statementText, int statementID) { return new NewStatementAction(statementText, statementID); }
	bool Execute(const float ticks = 0);
private:
	std::string statementText;
	int statementID;
};

class EndStatementAction : public DialogueAction {
public:
	EndStatementAction() {};
	virtual ~EndStatementAction() {};
	static EndStatementAction* CreateObject() { return new EndStatementAction(); }
	bool Execute(const float ticks = 0);
private:
	std::string statementText;
	int statementID;
};

template<typename T>
class DCGreaterThanAction : public DCAction {
public:
	DCGreaterThanAction(T a, T b) : currentState(0), a(a), b(b) {};
	virtual ~DCGreaterThanAction() {};
	static DCGreaterThanAction* CreateObject(T& a, T& b) { return new DCGreaterThanAction(a, b); }
	bool Execute(const float ticks = 0) {
		result = a < b;
		Notify();
		return a < b;
	}
private:
	int currentState;
	float time;
	T a;
	T b;
	int result;
};

template<typename T>
class DCLowerThanAction : public DCAction {
public:
	DCLowerThanAction(T a, T b) : currentState(0), a(a), b(b) {};
	virtual ~DCLowerThanAction() {};
	static DCLowerThanAction* CreateObject(T& a, T& b) { return new DCLowerThanAction(a, b); }
	bool Execute(const float ticks = 0) {
		result = a > b;
		Notify();
		return a > b;
	}

private:
	int currentState;
	float time;
	T a;
	T b;
	int result;
};

template<typename T>
class DCEqualToAction : public DCAction {
public:
	DCEqualToAction(T a, T b) : currentState(0), a(a), b(b) {};
	virtual ~DCEqualToAction() {};
	static DCEqualToAction* CreateObject(T& a, T& b) { return new DCEqualToAction(a, b); }
	bool Execute(const float ticks = 0) {
		result = a == b;
		Notify();
		return a == b;
	}
private:
	int currentState;
	float time;
	T a;
	T b;
	int result;
};


class ReadNextLineAction : public DialogueAction {
public:
	ReadNextLineAction(){};
	virtual ~ReadNextLineAction() {};
	static ReadNextLineAction* CreateObject() { return new ReadNextLineAction(); }
	bool Execute(const float ticks = 0);
private:
	int currentState;
	float time;
};

class EndReadingDialogueAction : public DialogueAction {
public:
	EndReadingDialogueAction() {};
	virtual ~EndReadingDialogueAction() {};
	static EndReadingDialogueAction* CreateObject() { return new EndReadingDialogueAction(); }
	bool Execute(const float ticks = 0);
private:
	int currentState;
	float time;
};