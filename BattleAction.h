#pragma once
#undef CreateEvent

enum class BattleActions { StartBattle, EndBattle, Delay, Attack, StandardAttack, CriticalAttack, MissedAttack, TakeDamage, CheckWin, AddExp, CounterAttack, ChangeActor, TakeGold, TakeItems, CheckCounterAttack, NextRound, LevelUp, ResetStats};

// Abstract class for Action
class BattleAction {
public:
	BattleAction() {}
	BattleAction(BattleActions ID) : actionID(ID) {};
	virtual ~BattleAction() {};
	static BattleAction* CreateObject();
	virtual bool Execute(const float ticks = 0) = 0;
private:
	BattleActions actionID;
};


