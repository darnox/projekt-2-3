#include "Dialogue.h"
#include <fstream>



Dialogue::Dialogue(){}


Dialogue::~Dialogue(){}

void Dialogue::MakeConnections(){
	for (auto statement : statements) {
		std::vector<int>* IDs = statement->getTmpNext();
		for (auto currentID : *IDs) {
			Statement* tmpStatement = FindStatementWithID(currentID);
			statement->AddNext(tmpStatement);
		}

	}
}

Statement* Dialogue::FindStatementWithID(int id) {
	for (auto statement : statements) {
		if (statement->getID() == id) return statement;
	}
	return NULL;
}

