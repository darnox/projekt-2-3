#pragma once
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <vector>


template <class TKey, class TType>
class Factory {	
	typedef std::unordered_map<TKey, void*> FactoryMap;

	Factory() {}
	FactoryMap FactMap;

public:
	//Singleton implementation
	static Factory <TKey, TType> & getInstance() {
		static Factory <TKey, TType> instance;
		return instance;
	}
	Factory(Factory const&) = delete;
	void operator=(Factory const&) = delete;

	//Register object
	template<typename F>
	void Register(const TKey &keyName, F func) {
		FactMap[keyName] = reinterpret_cast<void*>(func);
	}

	//Create object
	template<typename... TArgs>
	TType* CreateObj(const TKey &keyName, TArgs ...args) {
		keyName;
		auto it = FactMap.find(keyName);
		auto end = FactMap.end();
		if (it == end) std::exit(EXIT_FAILURE);

		typedef TType* (*CreateT)(TArgs...);
		auto createFn = reinterpret_cast<CreateT>(it->second);

		return createFn(std::forward<TArgs>(args)...);
	}
};

