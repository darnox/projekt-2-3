#include "DialogueMgr.h"
#include "src/statblock.h"
#include <functional>
#include "DialogueActions.h"
#include <fstream>
#include "src\filepathmgr.h"
#include "src\windowmgr.h"

DialogueMgr::DialogueMgr() {
	ActionFactory::getInstance().Register(DialogueActions::NewStatement, &NewStatementAction::CreateObject);
	ActionFactory::getInstance().Register(DialogueActions::EndStatement, &EndStatementAction::CreateObject); 
	ActionFactory::getInstance().Register(DialogueActions::ReadNextLine, &ReadNextLineAction::CreateObject);
	ActionFactory::getInstance().Register(DialogueActions::EndReadingDialogue, &EndReadingDialogueAction::CreateObject);
}

void DialogueMgr::Update(const float ticks){
	if (!isActive) return;

	if (currentAction) {
		bool actionStatus = currentAction->Execute(ticks); // Execute action
		if (actionStatus) return;  // Wait
		else delete currentAction; // Remove current action
		currentAction = NULL;
	}

	if (actionsList.size() > 0) { // Get another action
		currentAction = actionsList.front();
		actionsList.pop();
	}

	
}

void DialogueMgr::AddAction(DialogueAction* action) {
	actionsList.push(action);
}

int DialogueMgr::StartDialogue() {
	player = &ActorMgr::Instance().GetActivePlayer();
	companion = player->getActorInFront();

	if (!companion) return 1;
	std::string dialogueFile = companion->GetDialogueFile();
	if (dialogueFile == "") return 2;

	isActive = 1;

	dialogue = new Dialogue;
	StratReadingDialogue(dialogueFile);

	return 0;
}

int DialogueMgr::ReadFileLine() {
	
	std::string buffer;
	
	if (!getline(infile, buffer)) {
		AddAction(ActionFactory::getInstance().CreateObj(DialogueActions::EndReadingDialogue));
		return 1;
	}
	tokenizer.SetString(buffer);
	
	if (tokenizer.GetNextToken(currentToken)){
		if (currentToken.GetType() == TokenID::Identifier)
		{
			AddCommandFromToken();
		}
	}

	AddAction(ActionFactory::getInstance().CreateObj(DialogueActions::ReadNextLine));

	return 0;
}

void DialogueMgr::StratReadingDialogue(std::string dialogueFile) {
	std::string path = FilePathMgr::Instance().GetDialoguePath() + dialogueFile;
	infile.open(path);
	if (!(infile.is_open())) exit(0);

	AddAction(ActionFactory::getInstance().CreateObj(DialogueActions::ReadNextLine));

	isReading = 1;
}

void DialogueMgr::EndReadingDialogue() {
	infile.close();
	isReading = 0;

	dialogue->MakeConnections();

	currentStatement = dialogue->getCurrentStatement();
}

void DialogueMgr::ShowDialogueText(std::string text) {
	WindowMgr::Instance().AddDialogueGrp(text);
}

void DialogueMgr::AddCommandFromToken() {

	// Start statement creation
	if (currentToken.AsString() == "BEGIN"){
		tokenizer.GetNextToken(currentToken);
		std::string statementText = currentToken.AsString();

		tokenizer.GetNextToken(currentToken);
		int statementID = currentToken.AsInt();

		AddAction(ActionFactory::getInstance().CreateObj<std::string, int>(DialogueActions::NewStatement, statementText, statementID));
		return;
	}

	// End statement creation
	if (currentToken.AsString() == "END") {
		AddAction(ActionFactory::getInstance().CreateObj(DialogueActions::EndStatement));
		return;
	}

	if (currentToken.AsString() == "NEXT") {
		//tokenizer.GetNextToken(currentToken);
		while (currentToken.AsString() != "ENDNEXT" && tokenizer.GetNextToken(currentToken)) {
			if (currentToken.AsString() == "ENDNEXT") break;
			tempStatement->AddTmpNext(currentToken.AsInt());
		}
	}

	// Start if block
	if (currentToken.AsString() == "IF") {
		std::vector<Token> container;
		while (currentToken.AsString() != "ENDIF" && tokenizer.GetNextToken(currentToken))
			container.push_back(currentToken);

		if (container[0].AsString() == "PLAYER") {
			if (container[1].AsString() == "LEVEL") {
				int lvl = player->GetStatBlock().GetLevel();
				if (container[2].AsString() == "=") {
					if(tempStatement)
						tempStatement->AddCondition(new DCEqualToAction<int>(lvl, container[3].AsInt()));
				}
				else if (container[2].AsString() == ">") {
					if (tempStatement)
					tempStatement->AddCondition(new DCGreaterThanAction<int>(lvl, container[3].AsInt()));
				}
				else if (container[2].AsString() == "<") {
					if (tempStatement)
					tempStatement->AddCondition(new DCLowerThanAction<int>(lvl, container[3].AsInt()));
				}
			}
			else if (container[1].AsString() == "INTELLIGENCE") {
				int intelligence = player->GetStatBlock().GetLevel(); // TODO: add intelligence
				if (container[2].AsString() == "=") {
					if (tempStatement)
					tempStatement->AddCondition(new DCEqualToAction<int>(intelligence, container[3].AsInt()));
				}
				else if (container[2].AsString() == ">") {
					if (tempStatement)
					tempStatement->AddCondition(new DCGreaterThanAction<int>(intelligence, container[3].AsInt()));
				}
				else if (container[2].AsString() == "<") {
					if (tempStatement)
					tempStatement->AddCondition(new DCLowerThanAction<int>(intelligence, container[3].AsInt()));
				}
			}
			else if (container[1].AsString() == "CLASS") {
				CharacterClass::Type clas = player->GetStatBlock().GetCharacterClass();
				std::string class_string = CharacterClass::Convert::EnumToName(clas);
				if (container[2].AsString() == "=") {
					if (tempStatement)
					tempStatement->AddCondition(new DCEqualToAction<std::string>(class_string, container[3].AsString()));
				}
				else if (container[2].AsString() == ">") {
					if (tempStatement)
					tempStatement->AddCondition(new DCGreaterThanAction<std::string>(class_string, container[3].AsString()));
				}
				else if (container[2].AsString() == "<") {
					if (tempStatement)
					tempStatement->AddCondition(new DCLowerThanAction<std::string>(class_string, container[3].AsString()));
				}
			}
		}		

		// Add actions
		if (currentToken.AsString() == "ACTION") {
			std::vector<Token> container;
			while (currentToken.AsString() != "ENDACTION" && tokenizer.GetNextToken(currentToken))
				container.push_back(currentToken);

			if (container[0].AsString() == "PLAYER") {
				if (container[1].AsString() == "GIVE") {
					if (container[2].AsString() == "SWORD") {
						// Add sword
					}
				}
			}
		}
		return;
	}
}


Statement** DialogueMgr::getTempStatement() {
	return &tempStatement;
}

Dialogue** DialogueMgr::getDialogue() {
	return &dialogue;
}

