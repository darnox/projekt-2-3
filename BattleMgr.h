#pragma once
#include "src\actor.h"
#include "src\actormgr.h"
#include "BattleActions.h"
#include "Factory.h"
#include <queue>
#include "src\battlecutscene.h"
#include "src\gamewindow.h"

class BattleMgr{
	typedef Factory<BattleActions, BattleAction> ActionFactory;
public: 
	//Singleton implementation
	static BattleMgr& getInstance(){
		static BattleMgr instance;
		return instance;
	}
	BattleMgr(BattleMgr const&) = delete;
	void operator=(BattleMgr const&) = delete;

	void Update(const float ticks);
	int StartBattle();
	void AddAction(BattleAction* action);
	void EndBattle();
	bool IsBattleActive() { return isActive; }

	void ChangeActive();

	Actor* getPlayer() { return player; }
	Actor* getEnemy() { return enemy; }
	Actor* getActivePlayer() { return activeActor; }
	Actor* getOponentPlayer() { 
		if (activeActor == player) return enemy;
		return player;
	}

	void ShowCombatText(const std::string& text);
	void HideCombatText();
	AutoAdvanceWindow* GetCombatText();
	void SetActorPose(const Actor* actor, const BattleAniFrames::Type pose);

	void RemoveLoser(Actor* actor);

private:
	BattleMgr();

	std::queue<BattleAction*> actionsList;

	Actor* player;
	Actor* enemy;
	Actor* activeActor;

	bool isActive;
	BattleCutscene* cutScene;
	AutoAdvanceWindow* autoWin;
	BattleAction * currentAction;
	GameObject inputBlocker;
	GameString bigText;
};

