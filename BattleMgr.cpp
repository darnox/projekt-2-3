#include "BattleMgr.h"
#include "src\actor.h"
#include "src\audiomgr.h"
#include "src\windowmgr.h"
#include "src\gameobjectmgr.h"
#include "src\tilemap.h"
#include "src\actormgr.h"
#include "src\statblock.h"
#include "src\stlex.h"
#include "src\commandmgr.h"

BattleMgr::BattleMgr() : player(NULL), enemy(NULL), activeActor(NULL), isActive(0), cutScene(NULL), currentAction(NULL) {
	//Register battle actions
	ActionFactory::getInstance().Register(BattleActions::StartBattle, &StartBattleAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::EndBattle, &EndBattleAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::Delay, &DelayAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::Attack, &AttackAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::AddExp, &AddExpAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::ChangeActor, &ChangeActorAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::CheckWin, &CheckWinAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::CounterAttack, &CounterAttackAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::CheckCounterAttack, &CheckCounterAttackAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::CriticalAttack, &CriticalAttackAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::MissedAttack, &MissedAttackAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::StandardAttack, &StandardAttackAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::TakeDamage, &TakeDamageAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::NextRound, &NextRoundAction::CreateObject);
	//ActionFactory::getInstance().Register(BattleActions::TakeGold, &TakeGoldAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::TakeItems, &TakeItemsAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::LevelUp, &LevelUpAction::CreateObject);
	ActionFactory::getInstance().Register(BattleActions::ResetStats, &ResetStatsAction::CreateObject);
};

int BattleMgr::StartBattle() {
	player = &ActorMgr::Instance().GetActivePlayer();
	enemy = player->getActorInFront();
	activeActor = player;
	if (!enemy) return 1;
	else if (enemy->GetTeam() != ActorTeam::Enemy) return 2;

	isActive = 1;

	//Start CutScene
	WindowMgr::Instance().DismissLastModelessWindow(true);
	WindowMgr::Instance().AddBattleCutsceneHUDGrp(player, enemy);
	cutScene = new BattleCutscene(player, enemy);
	GameObjectMgr::Instance().AddObject(*cutScene);

	AddAction(ActionFactory::getInstance().CreateObj<float>(BattleActions::StartBattle, 2000));

	return 0;
}

void BattleMgr::EndBattle() {
	bool end = 0;
	if (!player->GetStatBlock().IsAlive()) {
		AudioMgr::PlaySong("gameover.mp3");
		AudioMgr::LoopSong(false);
		CommandMgr::Instance().AddCommandManually(new GCShowOutroAndQuit());
	}
	if (!enemy->GetStatBlock().IsAlive()) {
		RemoveLoser(enemy);
	}

	player = NULL;
	enemy = NULL;
	activeActor = NULL;
	currentAction = NULL;

	isActive = 0;

	GameObjectMgr::Instance().RemoveObject(*cutScene);
	delete cutScene;
	cutScene = NULL;

	GameObjectMgr::Instance().PopFocusObject();
	GameObjectMgr::Instance().RemoveObject(inputBlocker);

	WindowMgr::Instance().DismissLastModelessWindow();

}

void BattleMgr::Update(const float ticks) {
	if (!isActive) return;
	
	if (currentAction) {
		bool actionStatus = currentAction->Execute(ticks); // Execute action
		if (actionStatus) return;  // Wait
		else delete currentAction; // Remove current action
		currentAction = NULL;
	}

	if (actionsList.size() > 0) { // Get another action
		currentAction = actionsList.front();
		actionsList.pop();
	}
}

void BattleMgr::AddAction(BattleAction* action){
	actionsList.push(action);
}

void BattleMgr::ShowCombatText(const std::string& text){
	assert(cutScene != NULL);
	if (!autoWin) autoWin = new AutoAdvanceWindow;
	GameObjectMgr::Instance().RemoveObject(*autoWin);
	
	(*autoWin).SetText(text);
	GameObjectMgr::Instance().AddObject(*autoWin);
	GameObjectMgr::Instance().FocusOnObject(autoWin);
}

void BattleMgr::HideCombatText(){
	if (GameObjectMgr::Instance().CheckCurrentFocus(autoWin))
	{
		GameObjectMgr::Instance().PopFocusObject();
		GameObjectMgr::Instance().RemoveObject(*autoWin);
		autoWin = NULL;
	}
}

void BattleMgr::RemoveLoser(Actor* actor) {
	ActorMgr::Instance().RemoveActor(actor);
}

AutoAdvanceWindow* BattleMgr::GetCombatText(){
	return autoWin;
}

void BattleMgr::SetActorPose(const Actor* actor, const BattleAniFrames::Type pose){
	assert(cutScene != NULL);
	if(actor == player) cutScene->SetPlayerPose(pose);
	else cutScene->SetEnemyPose(pose);
}

void BattleMgr::ChangeActive() {
	if (activeActor == player) activeActor = enemy;
	else activeActor = player;
}