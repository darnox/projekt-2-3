#pragma once
#include "DialogueAction.h"
#include "Observer.h"

class DCAction: public DialogueAction, public Subject{
public:
	DCAction() {};
	~DCAction() {};

	int getConditionStatus(){ return result; }

protected:
	int result;
};

