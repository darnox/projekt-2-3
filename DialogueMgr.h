#pragma once
#include "src\actor.h"
#include "src\actormgr.h"
#include "Factory.h"
#include <queue>
#include "src\gamewindow.h"
#include "DialogueActions.h"
#include "src\actormgr.h"
#include "src\tokenizer.h"
#include "Dialogue.h"

class DialogueMgr {
	typedef Factory<DialogueActions, DialogueAction> ActionFactory;
public:
	//Singleton implementation
	static DialogueMgr& getInstance() {
		static DialogueMgr instance;
		return instance;
	}
	DialogueMgr(DialogueMgr const&) = delete;
	void operator=(DialogueMgr const&) = delete;

	void Update(const float ticks);
	int StartDialogue();
	void AddAction(DialogueAction* action);
	void EndDialogue();
	bool IsDialogueActive() { return isActive; }

	void AddCommandFromToken();


	void ShowDialogueText(const std::string& text);
	void HideDialogueText();
	AutoAdvanceWindow* GetDialogueText();

	Statement** getTempStatement();
	Dialogue** getDialogue();
	int ReadFileLine();
	void StratReadingDialogue(std::string dialogueFile);
	void EndReadingDialogue();

	void ShowDialogueText(std::string text);

private:
	DialogueMgr();

	Actor* player;
	Actor* companion;

	std::queue<DialogueAction*> actionsList;

	bool isActive;
	bool isReading = 0;

	AutoAdvanceWindow* autoWin;
	DialogueAction * currentAction;
	GameString bigText;

	Tokenizer tokenizer;
	Token currentToken;

	Statement* currentStatement;
	Statement* tempStatement;
	Dialogue* dialogue;
	std::ifstream infile;
};


